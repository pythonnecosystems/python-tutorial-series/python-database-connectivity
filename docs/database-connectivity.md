# Python 데이터베이스 연결: SQLite, MySQL

## <a name="intro"></a> 개요
Python 데이터베이스 연결에 관한 이 포스팅에서는 Python을 사용하여 SQLite과 MySQL 데이터베이스와 연결하고 상호 작용하는 방법을 설명한다. 또한 SQL 쿼리와 Python 명령을 사용하여 데이터를 생성하고, 읽고, 업데이트하고, 삭제하는 방법도 설명한다.

Python은 웹 개발, 데이터 분석, 머신 러닝 등 다양한 용도로 사용될 수 있는 인기 있고 다재다능한 프로그래밍 언어이다. Python에는 SQLite3라는 모듈도 내장되어 있다. SQLite 데이터베이스는 데이터를 하나의 파일에 저장하는 가볍고 독립적인 데이터베이스이다. SQLite 데이터베이스는 프로토타이핑, 테스트 또는 임베디드 시스템과 같은 소규모 응용 프로그램에 이상적이다.

그러나 웹 애플리케이션, 전자 상거래 또는 데이터 웨어하우징에 사용되는 데이터베이스 같이 더 크고 복잡한 데이터베이스를 사용해야 하는 경우 여러 사용자, 트랜잭션 및 동시성을 지원하는 널리 사용되고 강력한 관계형 데이터베이스 관리 시스템(RDBMS)인 MySQL을 사용하는 것이 좋다. MySQL 데이터베이스는 서버에 저장되며 이를 액세스하기 위해 별도의 클라이언트 프로그램이 필요하다.

Python을 사용하여 MySQL 데이터베이스와 연결하고 상호 작용하려면 MySQL 서버에 Python 인터페이스를 제공하는 `mysql-connector-python`이라는 서드 파티 모듈을 설치해야 한다. Python 패키지를 설치하고 관리하는 도구인 `pip` 명령을 사용하여 이 모듈을 설치할 수 있다.

이 포스팅에서는 다음과 같은 방법을 학습한다.

- Python을 사용하여 SQLite와 MySQL 데이터베이스에 연결
- Python을 사용하여 SQL 쿼리 실행
- Python을 사용하여 데이터 생성, 읽기, 업데이트 및 삭제

이 포스팅을 학습하고 나면 Python을 사용하여 SQLite 및 MySQL 데이터베이스에 대하여 작업하는 방법을 잘 이해할 수 있을 것이다. 또한 학습한 기술과 개념을 자신의 프로젝트와 작업에 적용할 수 있을 것이다.

시작합시다!

## <a name="sec_02"></a> 데이터베이스란?
Python을 이용하여 SQLite와 MySQL 데이터베이스를 연결하고 상호 작용하는 방법에 대해 자세히 살펴보기 전에 먼저 데이터베이스가 무엇인지, 왜 데이터베이스가 필요한지에 대해 알아보자.

데이터베이스는 데이터에 대하여 저장, 접근, 조작, 분석을 쉽게 할 수 있도록 정리된 데이터의 집합체이다. 데이터는 숫자, 텍스트, 이미지, 동영상 등 의미 있고 유용한 모든 것이 될 수 있다. 예를 들어, 데이터베이스는 고객, 제품, 주문, 거래 등에 관한 정보를 저장할 수 있다.

데이터베이스에는 관계형(relational), 계층형(hierarchical), 네트워크형(network), 객체지향형(object-oriented), 문서형(document), 그래프형(graph) 등 다양한 종류가 있다. 각 타입은 데이터의 성격과 구조에 따라 고유한 장단점을 가지고 있다. 본 포스팅에서는 가장 보편적이고 널리 사용되는 데이터베이스 종류인 관계형 데이터베이스에 촛점을 두고자 한다.

관계형 데이터베이스는 데이터를 테이블로 정리하는 데이터베이스이며, 여기서 각 테이블은 행과 열로 구성된다. 각 행은 레코드 또는 엔티티를 나타내며, 각 열은 엔티티의 어트리뷰트 또는 속성을 나타낸다. 예를 들어, `customer`라는 테이블은 `id`, `name`, `email`, `phone` 등의 열을 가질 수 있으며, 테이블의 각 행은 단일 고객의 정보를 저장할 수 있다.

관계형 데이터베이스에서 작업하기 위해서는 관계형 데이터베이스에서 데이터를 생성, 질의, 업데이트, 삭제하기 위한 표준적이고 보편적인 언어인 SQL(Structured Query Language)이라는 언어가 필요하다. SQL을 사용하면 데이터에 대한 선택, 필터링, 정렬, 그룹화, 집계, 어그리게이션(aggregation) 등 다양한 작업을 수행할 수 있다. SQL을 사용하면 테이블 생성, 테이블 변경, 인덱스 추가 등 데이터의 구조와 제약 조건을 정의할 수도 있다.

관계형 데이터베이스를 생성하고 관리할 수 있는 기능과 인터페이스를 제공하는 소프트웨어 어플리케이션인 관계형 데이터베이스 관리 시스템(RDBMS)의 두 가지 예로 SQLite와 MySQL을 들 수 있다. SQLite와 MySQL은 유사점과 차이점을 가지고 있다. 이에 대해서는 다음 절에서 살펴볼 것이다.

## <a name="sec_03"></a> SQLite 데이터베이스
이 절에서는 SQLite 데이터베이스와 이를 Python에서 사용하는 방법에 대해 알아본다. SQLite는 독립형, 서버리스, 제로 구성 데이터베이스 엔진으로 하나의 파일에 데이터를 저장한다. SQLite는 설치, 설정, 관리가 전혀 필요하지 않기 때문에 프로토타이핑, 테스트 또는 임베디드 시스템과 같은 소규모 응용 프로그램에 이상적이다. SQLite는 또한 테이블 생성, 쿼리 실행, 트랜잭션 수행과 같은 대부분의 SQL 기능을 지원한다.

Python에서 SQLite를 사용하려면 SQLite 데이터베이스에 Python 인터페이스를 제공하는 내장 모듈인 `sqlite3` 모듈을 임포트해야 한다. sqlite3 모듈을 사용하면 데이터베이스 파일을 나타내는 연결 객체와 데이터베이스에서 SQL 명령을 실행할 수 있는 커서 객체를 만들 수 있다. sqlite3 모듈을 사용하여 오류를 처리하고 변경 사항을 커밋하며 연결을 닫을 수도 있다.

다음은 sqlite3 모듈을 사용하여 `test.db`라는 SQLite 데이터베이스 파일과 `id`, `name`, `score` 세 개의 열을 가진 `student`라는 테이블을 만드는 방법의 예이다.

```python
# Import the sqlite3 module
import sqlite3

# Create a connection object that represents the database file
conn = sqlite3.connect("test.db")
# Create a cursor object that allows you to execute SQL commands
cur = conn.cursor()
# Create a table called students with three columns: id, name, and score
cur.execute("CREATE TABLE students (id INTEGER PRIMARY KEY, name TEXT, score REAL)")
# Commit the changes to the database
conn.commit()
# Close the connection
conn.close()
```

아래와 같이 `with` 문을 사용하여 연결을 자동으로 커밋하고 닫을 수도 있다.

```python
# Import the sqlite3 module
import sqlite3

# Use the with statement to automatically commit and close the connection
with sqlite3.connect("test.db") as conn:
    # Create a cursor object that allows you to execute SQL commands
    cur = conn.cursor()
    # Create a table called students with three columns: id, name, and score
    cur.execute("CREATE TABLE students (id INTEGER PRIMARY KEY, name TEXT, score REAL)")
```

향후 Python과 sqlite3 모듈을 사용하여 SQL 쿼리를 실행하는 방법에 대해 알아볼 것이다.

## <a name="sec_04"></a> MySQL 데이터베이스
이번 절에서는 Python에서 MySQL 데이터베이스를 사용하는 방법에 대해 알아본다. MySQL은 강력하고 널리 사용되는 관계형 데이터베이스 관리 시스템(RDBMS)으로 여러 사용자, 트랜잭션 및 동시성을 지원한다. MySQL은 고성능, 확장성 및 보안을 제공하므로 웹 애플리케이션, 전자 상거래 또는 데이터 웨어하우징과 같은 대규모 및 복잡한 어플리케이션에서도 사용할 수 있이다. 또한 MySQL은 테이블 생성, 쿼리 실행, 트랜잭션 수행 같은 대부분의 SQL 기능을 지원한다.

Python에서 MySQL을 사용하려면 MySQL 서버에 Python 인터페이스를 제공하는 `mysql-connector-python`이라는 서드 파티 모듈을 설치해야 한다. Python 패키지를 설치하고 관리하는 도구인 `pip` 명령을 사용하여 이 모듈을 설치할 수 있다. 모듈을 설치하려면 터미널에서 다음 명령을 실행하면 된다.

```python
# Install the mysql-connector-python module using pip
pip install mysql-connector-python
```

모듈을 설치한 후 Python 프로그램에서 임포트하여 MySQL 서버를 나타내는 연결 객체와 데이터베이스에서 SQL 명령을 실행할 수 있는 커서 객체를 만들 수 있다. 모듈을 사용하여 오류를 처리하고 변경 사항을 커밋한 다음 연결을 닫을 수도 있다.

다음은 `mysql-connector-python` 모듈을 사용하여 MySql 서버에 연결하여 `id`, `name`, `score` 세 개의 열로 이루어진 `test`라는 데이터베이스와 `student`라는 테이블을 만드는 코드의 예이다.

```python
# Import the mysql.connector module
import mysql.connector

# Create a connection object that represents the MySQL server
conn = mysql.connector.connect(
    host="localhost", # The host name or IP address of the MySQL server
    user="root", # The user name for the MySQL server
    password="password" # The password for the MySQL server
)
# Create a cursor object that allows you to execute SQL commands
cur = conn.cursor()
# Create a database called test
cur.execute("CREATE DATABASE test")
# Use the test database
cur.execute("USE test")
# Create a table called students with three columns: id, name, and score
cur.execute("CREATE TABLE students (id INT PRIMARY KEY, name VARCHAR(255), score FLOAT)")
# Commit the changes to the database
conn.commit()
# Close the connection
conn.close()
```

아래와 같이 `with` 문을 사용하여 연결을 자동으로 커밋하고 닫을 수도 있다.

```python
# Import the mysql.connector module
import mysql.connector

# Use the with statement to automatically commit and close the connection
with mysql.connector.connect(
    host="localhost", # The host name or IP address of the MySQL server
    user="root", # The user name for the MySQL server
    password="password" # The password for the MySQL server
) as conn:
    # Create a cursor object that allows you to execute SQL commands
    cur = conn.cursor()
    # Create a database called test
    cur.execute("CREATE DATABASE test")
    # Use the test database
    cur.execute("USE test")
    # Create a table called students with three columns: id, name, and score
    cur.execute("CREATE TABLE students (id INT PRIMARY KEY, name VARCHAR(255), score FLOAT)")
```

향후 Python과 mysql-connector-python 모듈을 사용하여 SQL 쿼리를 실행하는 방법에 대해 알아본다.

## <a name="sec_05"></a> Python 데이터베이스 API
본 절에서는 Python을 이용하여 다양한 데이터베이스와 연결하고 상호 작용하기 위한 표준적이고 균일한 인터페이스인 Python 데이터베이스 API에 대해 알아본다. Python 데이터베이스 API는 연결 생성, 쿼리 실행, 결과 가져오기, 변경 사항 커밋, 연결 닫기 등과 같은 공통적인 데이터베이스 작업을 수행할 수 있는 일련의 메서드와 객체를 정의한다. 또한 Python 데이터베이스 API는 오류와 경고를 처리하는 데 사용할 수 있는 몇 가지 예외와 오류 코드를 정의한다.

Python 데이터베이스 API는 SQLite의 경우 `sqlite3`, MySQL의 경우 `mysql-connector-python` 같이 특정 데이터베이스에 대한 기능과 인터페이스를 제공하는 서로 다른 모듈에 의해 구현된다. 이러한 모듈은 Python과 데이터베이스 사이의 브리지 역할을 하므로 데이터베이스 드라이버 또는 어댑터라고도 한다. 각 모듈은 데이터베이스에 고유한 추가 기능이나 변형을 가질 수 있지만 모두 Python 데이터베이스 API의 기본 구조와 구문을 따른다.

Python 데이터베이스 API는 크게 `connection` 객체와 `cursor` 객체의 두 가지 요소로 구성된다. connection 객체는 데이터베이스 서버와의 연결을 나타내며, 연결 생성과 종결, 변경 사항 커밋과 롤백, cursor 객체 생성 메서드를 제공한다. cursor 객체는 데이터베이스 cursor를 나타내며, 이는 SQL 명령어를 실행하고 결과를 가져올 수 있는 제어 구조이다. cursor 객체는 쿼리를 실행하고, 그 결과를 제공하고 그리고 그 결과에서 반복할 수 있는 메서드를 제공한다.

다음은 Python 데이터베이스 API를 사용하여 SQLite 데이터베이스에 연결하고 간단한 쿼리를 실행하는 방법의 예이다.

```python
# Import the sqlite3 module
import sqlite3

# Create a connection object that represents the database file
conn = sqlite3.connect("test.db")
# Create a cursor object that allows you to execute SQL commands
cur = conn.cursor()
# Execute a simple query that selects all records from the students table
cur.execute("SELECT * FROM students")
# Fetch all the results as a list of tuples
results = cur.fetchall()
# Print the results
for row in results:
    print(row)
# Close the cursor
cur.close()
# Close the connection
conn.close()
```

다음은 Python 데이터베이스 API를 사용하여 MySQL 데이터베이스에 연결하고 간단한 쿼리를 실행하는 방법의 예이다.

```python
# Import the mysql.connector module
import mysql.connector

# Create a connection object that represents the MySQL server
conn = mysql.connector.connect(
    host="localhost", # The host name or IP address of the MySQL server
    user="root", # The user name for the MySQL server
    password="password", # The password for the MySQL server
    database="test" # The name of the database to use
)
# Create a cursor object that allows you to execute SQL commands
cur = conn.cursor()
# Execute a simple query that selects all records from the students table
cur.execute("SELECT * FROM students")
# Fetch all the results as a list of tuples
results = cur.fetchall()
# Print the results
for row in results:
    print(row)
# Close the cursor
cur.close()
# Close the connection
conn.close()
```

보다시피 Python 데이터베이스 API는 Python을 이용하여 서로 다른 데이터베이스와 일관되고 쉽게 작업할 수 있는 수단을 제공한다. 다음 절에서는 Python 데이터베이스 API를 이용하여 데이터를 생성, 읽기, 업데이트, 삭제 등 데이터에 대한 다양한 연산을 수행하는 방법에 대해 알아본다.

## <a name="sec_06"></a> Python에서 SQLite 데이터베이스에 연결
본 절에서는 Python과 sqlite3 모듈을 사용하여 SQLite 데이터베이스에 연결하는 방법에 대해 알아본다. 앞 절에서 같이 SQLite는 단일 파일에 데이터를 저장하는 독립형, 서버리스, 제로 구성 데이터베이스 엔진이다. Python과 함께 SQLite를 사용하기 위해서는 SQLite 데이터베이스에 Python 인터페이스를 제공하는 내장 모듈인 `sqlite3` 모듈을 임포트하여야 한다.

sqlite3 모듈을 사용하여 데이터베이스 파일을 나타내는 `connection` 객체와 데이터베이스에서 SQL 명령을 실행할 수 있는 `cursor` 객체를 만들 수 있다. sqlite3 모듈을 사용하여 오류를 처리하고, 변경 사항을 커밋하며, connection을 닫을 수도 있다.

`connection` 객체를 만들려면 sqlite3 모듈의 `connection` 메서드를 사용하고 데이터베이스 파일의 이름을 인수로 전달해야 한다. 파일이 존재하지 않으면 자동으로 파일이 생성된다. 예를 들어 다음 코드는 `test.db`라는 데이터베이스 파일을 나타내는 `connection` 객체를 생성한다.

```python
# Import the sqlite3 module
import sqlite3

# Create a connection object that represents the database file
conn = sqlite3.connect("test.db")
```

cursor 객체를 만들려면 connection 객체의 `cursor` 메서드 사용해야 하는데, 이는 SQL 명령어를 실행할 때 사용할 수 있는 cursor 객체를 반환한다. 예를 들어 다음 코드는 `connection` 객체로부터 `cursor` 객체를 생성한다.

```python
# Create a cursor object that allows you to execute SQL commands
cur = conn.cursor()
```

SQL 명령어를 실행하기 위해서는 cursor 객체의 `execute` 메서드를 사용해야 하며, SQL 명령어를 문자열 인수로 전달해야 한다. 예를 들어, 다음 코드는 `id`, `name`, `score`의 세 열로 구성된 `students`라는 테이블을 만드는 SQL 명령어를 실행한다.

```python
# Execute a SQL command that creates a table called students
cur.execute("CREATE TABLE students (id INTEGER PRIMARY KEY, name TEXT, score REAL)")
```

데이터베이스에 변경 사항을 커밋하려면, 데이터베이스 파일에 변경 사항을 저장하는 `connection` 객체의 `commit` 메서드를 사용해야 한다. 예를 들어, 다음 코드는 데이터베이스에 변경 사항을 커밋한다.

```python
# Commit the changes to the database
conn.commit()
```

연결을 닫으려면 `connection` 객체의 `close` 메서드를 사용해야 하는데, 이 메서드는 연결을 닫고 자원을 릴리스한다. 예를 들어 다음 코드는 연결을 닫는다.

```python
# Close the connection
conn.close()
```

다음과 같이 `with` 문을 사용하여 연결을 자동으로 커밋하고 닫을 수도 있다.

```python
# Use the with statement to automatically commit and close the connection
with sqlite3.connect("test.db") as conn:
    # Create a cursor object that allows you to execute SQL commands
    cur = conn.cursor()
    # Execute a SQL command that creates a table called students
    cur.execute("CREATE TABLE students (id INTEGER PRIMARY KEY, name TEXT, score REAL)")
```

이제 Python과 sqlite3 모듈을 사용하여 SQLite 데이터베이스에 연결하는 방법을 알게 되었으므로 다음 섹션으로 이동하여 Python과 sqlite3 모듈을 사용하여 SQL 쿼리를 실행하는 방법에 대해 알아보겠다.

## <a name="sec_07"></a> Python에서 SQLite SQL 쿼리 실행
이 절에서는 Python과 sqlite3 모듈을 사용하여 SQL 쿼리를 실행하는 방법에 대해 설명한다. 앞 절에서 보인 바와 같이 sqlite3 모듈을 사용하면 데이터베이스에서 SQL 명령을 실행할 수 있는 cursor 객체를 만들 수 있다. cursor 객체의 `execute` 메서드를 사용하여 SELECT, INSERT, UPDATE, DELETE 등과 같은 SQL 쿼리를 실행할 수 있다.

`execute` 메서드는 SQL 쿼리를 포함하는 string 인수와 선택적으로 쿼리에 대한 매개 변수를 포함하는 튜플 인수를 취한다. 매개 변수는 사용자 입력에 무단(unauthorized) SQL 명령을 삽입하여 실행하려는 악의적인 시도인 SQL 주입(SQL injection) 공격을 방지하는 데 유용하다. 매개 변수는 "?" 같은 쿼리의 자리 표시자 또는 ":name" 같은 이름 있는 자리 표시자로 표시된다. 예를 들어 다음 코드는 매개 변수를 사용하여 학생 테이블에 레코드를 삽입하는 SQL 쿼리를 실행한다.

```python
# Execute a SQL query that inserts a record into the students table, using parameters
cur.execute("INSERT INTO students (id, name, score) VALUES (?, ?, ?)", (1, "Alice", 90.0))
```

`execute` 메서드는 쿼리의 결과를 가져오는 데 사용할 수 있는 `cursor` 객체를 반환한다. `cursor` 객체는 결과를 가져오기 위한 여러 가지 메서드(예: `fetchone`, `fetchmany`, `fetchall`)를 제공한다. `fetchone` 메서드는 결과의 다음 행을 튜플로 반환하거나 더 이상 행이 없으면 없음을 반환한다. `fetchmany` 메서드는 결과의 다음 `n` 행을 튜플의 리스트로 반환하며, 여기서 `n`은 기본값이 `1`인 선택적 인수이다. `fetchall` 메서드는 결과의 나머지 모든 행을 튜플의 리스트로 반환하거나 더 이상 행이 없으면 빈 리스트로 반환한다. 예를 들어 다음 코드는 학생 테이블에서 모든 레코드를 선택하는 SQL 쿼리를 실행하고 `fetchall` 메서드를 사용하여 결과를 출력한다.

```python
# Execute a SQL query that selects all records from the students table
cur.execute("SELECT * FROM students")
# Fetch all the results as a list of tuples
results = cur.fetchall()
# Print the results
for row in results:
    print(row)
```

cursor 객체는 반복도 지원한다. 이는 결과의 행들에 대해 페치 방법을 사용하지 않고 반복을 위해 `for` 루프를 사용할 수 있음을 의미한다. 예를 들어, 다음 코드는 이전 코드와 동일한 동작을 하지만 반복을 사용한다.

```python
# Execute a SQL query that selects all records from the students table
cur.execute("SELECT * FROM students")
# Iterate over the rows of the result
for row in cur:
    print(row)
```

다음 절에서는 Python과 sqlite3 모듈을 이용하여 데이터를 생성, 읽기, 업데이트, 삭제하는 방법에 대해 알아본다.

## <a name="sec_08"></a> Python에서 SQLite 데이터 생성, 읽기, 업데이트 및 삭제
이 절에서는 Python과 sqlite3 모듈을 사용하여 데이터를 생성, 읽기, 업데이트, 삭제하는 방법에 대해서 설명한다. 앞 절에서 보인 것과 같이 sqlite3 모듈은 cursor 객체의 `execute` 메서드를 이용하여 데이터베이스 상에서 SQL 명령을 실행할 수 있다. `execute` 메서드를 사용하여 레코드의 삽입, 읽기, 업데이트, 삭제 등 데이터에 대해 SQL 연산을 수행할 수 있다.

테이블에 레코드를 삽입하려면, 삽입할 테이블 이름과 값을 지정하는 `INSERT` 문을 사용해야 한다. 앞 절에서 보인 것처럼 매개변수를 사용하여 값을 전달할 수 있다. 예를 들어, 다음 코드는 매개변수를 사용하여 학생 테이블에 레코드를 삽입한다.

```python
# Insert a record into the students table, using parameters
cur.execute("INSERT INTO students (id, name, score) VALUES (?, ?, ?)", (1, "Alice", 90.0))
```

표에서 레코드를 선택하려면 열과 선택할 테이블을 지정하는 `SELECT` 문을 사용해야 한다. 결과를 필터링, 정렬 또는 집계하기 위해 `WHERE`, `ORDER BY`, `GROUP BY` 등 옵션 절을 사용할 수 있다. 예를 들어, 다음 코드는 `students` 테이블에서 모든 레코드를 선택하고 `score` 내림차순으로 정렬한다.

```python
# Select all records from the students table, and order them by score in descending order
cur.execute("SELECT * FROM students ORDER BY score DESC")
```

테이블의 레코드를 업데이트하기 위해서는 `UPDATE` 문을 사용해야 하는데, `UPDATE` 문은 테이블 이름, 업데이트할 열, 새 값을 지정한다. `WHERE` 절을 사용하여 업데이트할 레코드를 지정할 수 있다. 예를 들어, 다음 코드는 `id`가 `1`인 학생의 `score`를 `95.0`으로 업데이트한다.

```python
# Update the score of the student with id 1 to 95.0
cur.execute("UPDATE students SET score = ? WHERE id = ?", (95.0, 1))
```

테이블에서 레코드를 삭제하기 위해서는 테이블 이름과 삭제할 레코드를 지정하는 `DELETE` 문을 사용해야 한다. `WHERE` 절을 사용하여 삭제할 레코드를 지정할 수 있다. 예를 들어, 다음 코드는 `id`가 `1`인 학생의 레코드를 삭제한다.

```python
# Delete the record of the student with id 1
cur.execute("DELETE FROM students WHERE id = ?", (1,))
```

INSERT, UPDATE, DELETE 등 데이터를 수정하는 SQL 명령을 실행한 후에는 앞 절에서 보인 것처럼 `conn` 객체의 커밋 방식을 사용하여 변경사항을 데이터베이스에 커밋해야 한다. 예를 들어 다음 코드로 데이터베이스에 변경사항을 커밋한다.

```python
# Commit the changes to the database
conn.commit()
```

이제 Python과 sqlite3 모듈을 사용하여 데이터를 생성하고, 읽고, 업데이트하고, 삭제하는 학습하였으므로 다음 절로 이동하여 Python과 `mysql-connector-python` 모듈을 사용하여 MySQL 데이터베이스에 연결하는 방법에 대해 알아보겠다.

## <a name="sec_09"></a> Python에서 MySQL 데이터베이스에 연결
본 절에서는 Python과 smysql-connector-python 모듈을 사용하여 MySQL 데이터베이스에 연결하는 방법에 대해 알아본다. 앞 절에서 기술하였듯이 MySQL은 다중 사용자, 트랜잭션 및 동시성을 지원하는 널리 사용되고 강력한 관계형 데이터베이스 관리 시스템(RDBMS)이다. MySQL 데이터베이스는 서버에 저장되며 이에 접근하기 위해서는 별도의 클라이언트 프로그램이 필요하다.

MySQL을 Python과 함께 사용하기 위해서는 MySQL 서버에 Python 인터페이스를 제공하는 `mysql-connector-python`이라는 서드파티 모듈을 설치해야 한다. Python 패키지를 설치하고 관리하는 도구인 `pip` 명령을 사용하여 이 모듈을 설치할 수 있다. 다음 명령은 `mysql-connector-python` 모듈을 설치한다.

```bash
# Install the mysql-connector-python module using the pip command
$ pip install mysql-connector-python
```

`mysql-connector-python` 모듈은 Python을 사용하여 데이터베이스들과 연결하고 상호 작용하기 위한 표준 인터페이스인 Python Database API를 구현한다. `mysql-connector-python` 모듈을 사용하면 MySQL 서버를 나타내는 `connection` 객체와 데이터베이스에서 SQL 명령을 실행할 수 있는 `cursor` 객체를 만들 수 있다. 또한 mysql-connector-python 모듈을 사용하여 오류를 처리하고 변경 사항을 커밋하며 `connection`을 닫을 수 있다.

`connection` 객체를 만들려면 `mysql.connector` 모듈의 `connect` 메서드를 사용하고, host name, user name, password 및 database name을 인수로 전달해야 한다. 예를 위하여 다음 코드는 localhost에서 실행 중인 MySQL 서버를 나타내는 connection 객체를 만들고 user name `root`, password `password` 및 database name `test`를 사용한다.

```python
# Import the mysql.connector module
import mysql.connector
# Create a connection object that represents the MySQL server
conn = mysql.connector.connect(
    host="localhost", # The host name or IP address of the MySQL server
    user="root", # The user name for the MySQL server
    password="password", # The password for the MySQL server
    database="test" # The name of the database to use
)
```

cursor 객체를 만들려면 connection 객체의 `cursor` 메서드 사용해야 하는데, 이는 SQL 명령어를 실행할 때 사용할 수 있는 cursor 객체를 반환한다. 예를 들어 다음 코드는 `connection` 객체로부터 `cursor` 객체를 생성한다.

```python
# Create a cursor object that allows you to execute SQL commands
cur = conn.cursor()
```

SQL 명령어를 실행하기 위해서는 cursor 객체의 `execute` 메서드를 사용해야 하며, SQL 명령어를 문자열 인수로 전달해야 한다. 예를 들어, 다음 코드는 `id`, `name`, `score`의 세 열로 구성된 `students`라는 테이블을 만드는 SQL 명령어를 실행한다.

```python
# Execute a SQL command that creates a table called students
cur.execute("CREATE TABLE students (id INT PRIMARY KEY, name VARCHAR(255), score FLOAT)")
```

다음 절에서는 Python과 mysql-connector-python 모듈을 사용하여 SQL 쿼리를 실행하는 방법에 대해 알아본다.

## <a name="sec_10"></a> Python에서 MySQL SQL 쿼리 실행
이 절에서는 Python과 mysql-connector-python 모듈을 사용하여 SQL 쿼리를 실행하는 방법에 대해 설명한다. 앞 절에서 보인 바와 같이 mysql-connector-python 모듈을 사용하면 데이터베이스에서 SQL 명령을 실행할 수 있는 cursor 객체를 만들 수 있다. cursor 객체의 `execute` 메서드를 사용하여 SELECT, INSERT, UPDATE, DELETE 등과 같은 SQL 쿼리를 실행할 수 있다.

`execute` 메서드는 SQL 쿼리를 포함하는 string 인수와 선택적으로 쿼리에 대한 매개 변수를 포함하는 튜플 인수를 취한다. 매개 변수는 사용자 입력에 무단(unauthorized) SQL 명령을 삽입하여 실행하려는 악의적인 시도인 SQL 주입(SQL injection) 공격을 방지하는 데 유용하다. 매개 변수는 "%" 같은 쿼리의 자리 표시자 또는 "%(name)s" 같은 이름 있는 자리 표시자로 표시된다. 예를 들어 다음 코드는 매개 변수를 사용하여 학생 테이블에 레코드를 삽입하는 SQL 쿼리를 실행한다.

```python
# Execute a SQL query that inserts a record into the students table, using parameters
cur.execute("INSERT INTO students (id, name, score) VALUES (%s, %s, %s)", (1, "Alice", 90.0))
```

`execute` 메서드는 쿼리의 결과를 가져오는 데 사용할 수 있는 `cursor` 객체를 반환한다. `cursor` 객체는 결과를 가져오기 위한 여러 가지 메서드(예: `fetchone`, `fetchmany`, `fetchall`)를 제공한다. `fetchone` 메서드는 결과의 다음 행을 튜플로 반환하거나 더 이상 행이 없으면 없음을 반환한다. `fetchmany` 메서드는 결과의 다음 `n` 행을 튜플의 리스트로 반환하며, 여기서 `n`은 기본값이 `1`인 선택적 인수이다. `fetchall` 메서드는 결과의 나머지 모든 행을 튜플의 리스트로 반환하거나 더 이상 행이 없으면 빈 리스트로 반환한다. 예를 들어 다음 코드는 학생 테이블에서 모든 레코드를 선택하는 SQL 쿼리를 실행하고 `fetchall` 메서드를 사용하여 결과를 출력한다.

```python
# Execute a SQL query that selects all records from the students table
cur.execute("SELECT * FROM students")
# Fetch all the results as a list of tuples
results = cur.fetchall()
# Print the results
for row in results:
    print(row)
```

cursor 객체는 반복도 지원한다. 이는 결과의 행들에 대해 페치 방법을 사용하지 않고 반복을 위해 `for` 루프를 사용할 수 있음을 의미한다. 예를 들어, 다음 코드는 이전 코드와 동일한 동작을 하지만 반복을 사용한다.

```python
# Execute a SQL query that selects all records from the students table
cur.execute("SELECT * FROM students")
# Iterate over the rows of the result
for row in cur:
    print(row)
```

다음 절에서는 Python과 mysql-connector-python 모듈을 이용하여 데이터를 생성, 읽기, 업데이트, 삭제하는 방법에 대해 알아본다.

## <a name="sec_11"></a> Python에서 MySQL  데이터 생성, 읽기, 업데이트 및 삭제
이 절에서는 Python과 mysql-connector-python 모듈을 사용하여 데이터를 생성, 읽기, 업데이트, 삭제하는 방법에 대해서 설명한다. 앞 절에서 보인 것과 같이 mysql-connector-python 모듈은 cursor 객체의 `execute` 메서드를 이용하여 데이터베이스 상에서 SQL 명령을 실행할 수 있다. `execute` 메서드를 사용하여 레코드의 삽입, 읽기, 업데이트, 삭제 등 데이터에 대해 SQL 연산을 수행할 수 있다.

테이블에 레코드를 삽입하려면, 삽입할 테이블 이름과 값을 지정하는 `INSERT` 문을 사용해야 한다. 앞 절에서 보인 것처럼 매개변수를 사용하여 값을 전달할 수 있다. 예를 들어, 다음 코드는 매개변수를 사용하여 학생 테이블에 레코드를 삽입한다.

```python
# Insert a record into the students table, using parameters
cur.execute("INSERT INTO students (id, name, score) VALUES (%s, %s, %s)", (1, "Alice", 90.0))
```

표에서 레코드를 선택하려면 열과 선택할 테이블을 지정하는 `SELECT` 문을 사용해야 한다. 결과를 필터링, 정렬 또는 집계하기 위해 `WHERE`, `ORDER BY`, `GROUP BY` 등 옵션 절을 사용할 수 있다. 예를 들어, 다음 코드는 `students` 테이블에서 모든 레코드를 선택하고 `score` 내림차순으로 정렬한다.

```python
# Select all records from the students table, and order them by score in descending order
cur.execute("SELECT * FROM students ORDER BY score DESC")
```

테이블의 레코드를 업데이트하기 위해서는 `UPDATE` 문을 사용해야 하는데, `UPDATE` 문은 테이블 이름, 업데이트할 열, 새 값을 지정한다. `WHERE` 절을 사용하여 업데이트할 레코드를 지정할 수 있다. 예를 들어, 다음 코드는 `id`가 `1`인 학생의 `score`를 `95.0`으로 업데이트한다.

```python
# Update the score of the student with id 1 to 95.0
cur.execute("UPDATE students SET score = %s WHERE id = %s", (95.0, 1))
```

테이블에서 레코드를 삭제하기 위해서는 테이블 이름과 삭제할 레코드를 지정하는 `DELETE` 문을 사용해야 한다. `WHERE` 절을 사용하여 삭제할 레코드를 지정할 수 있다. 예를 들어, 다음 코드는 `id`가 `1`인 학생의 레코드를 삭제한다.

```python
# Delete the record of the student with id 1
cur.execute("DELETE FROM students WHERE id = %s", (1,))
```

INSERT, UPDATE, DELETE 등 데이터를 수정하는 SQL 명령을 실행한 후에는 앞 절에서 보인 것처럼 `conn` 객체의 커밋 방식을 사용하여 변경사항을 데이터베이스에 커밋해야 한다. 예를 들어 다음 코드로 데이터베이스에 변경사항을 커밋한다.

```python
# Commit the changes to the database
conn.commit()
```

## <a name="summary"></a> 요약
이것으로 Python 데이터베이스 연결에 대한 포스팅을 마치겠다. Python과 sqlite3 및 mysql-connector-python 모듈을 사용하여 SQLite 및 MySQL 데이터베이스에 연결하고 상호 작용하는 방법을 학습하였다. 또한 SQL 쿼리와 Python 명령을 사용하여 데이터를 생성하고, 읽고, 업데이트하고, 삭제하는 방법도 살펴보았다. 
