# Python 데이터베이스 연결: SQLite, MySQL <sup>[1](#footnote_1)</sup>

> <font size="3">Python을 사용하여 SQLite와 MySQL 데이터베이스와 연결하고 상호 작용하는 방법에 대해 알아본다.</font>

## 목차

1. [개요](./database-connectivity.md#intro)
1. [데이터베이스란?](./database-connectivity.md#sec_02)
1. [SQLite 데이터베이스](./database-connectivity.md#sec_03)
1. [MySQL 데이터베이스](./database-connectivity.md#sec_04)
1. [Python 데이터베이스 API](./database-connectivity.md#sec_05)
1. [Python에서 SQLite 데이터베이스에 연결](./database-connectivity.md#sec_06)
1. [Python에서 SQLite SQL 쿼리 실행](./database-connectivity.md#sec_07)
1. [Python에서 SQLite 데이터 생성, 읽기, 업데이트 및 삭제](./database-connectivity.md#sec_08)
1. [Python에서 MySQL 데이터베이스에 연결](./database-connectivity.md#sec_09)
1. [Python에서 MySQL SQL 쿼리 실행](./database-connectivity.md#sec_10)
1. [Python에서 MySQL 데이터 생성, 읽기, 업데이트 및 삭제](./database-connectivity.md#sec_11)
1. [요약](./database-connectivity.md#summary)

<a name="footnote_1">1</a>: [Python Tutorial 39 — Python Database Connectivity: SQLite, MySQL](https://python.plainenglish.io/python-tutorial-39-python-database-connectivity-sqlite-mysql-29d0b0baf8d0?sk=ed47c4ef7cb9f5ea9ea0cdd9e3067334)를 편역하였습니다.
